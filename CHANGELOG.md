﻿# CHANGELOG

## [v3.8.1] 2021.11.23

- 新增 SameSite 特性防止 CSRF


## [v3.8.0] 2021.10.13

- 升级 springboot 2.5.5 依赖
- 废除 @Login 注解修改为 @LoginIgnore 注解
- 注解 @Permission 属性 action 修改为 ignore 默认 false

